package com.example.demo.payload.request;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
//закончить password matches
public class SignupRequest {

    @Email(message = "It doesn't look like email -_-")
    @NotBlank(message = "I want to see your email")
    private String email;
    @NotEmpty(message = "What is your name, stranger?")
    private String firstname;
    @NotEmpty(message = "And lastname, it's really important")
    private String lastname;
    @NotEmpty(message = "And now you can create some stupid nickname")
    private String username;
    @NotEmpty(message = "Password for protection from alien invaders")
    @Size(min = 6)
    private String password;
    private String confirmPassword;

}
